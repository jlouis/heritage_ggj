﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class FadingMixer : MonoBehaviour {

	public AudioMixer mixer; 
	public float strenght = 0.002f;
	public float maxAttenuation;
	float currentAttenuation;
	bool fading = false;
	bool right = true;
	bool left = false;

	void Start()
	{
		currentAttenuation = maxAttenuation;
		mixer.SetFloat ("MasterVolume", maxAttenuation);
		EventHandler.AddEventListener (EventHandler.GameEvent.Fading, Fade);
		Fade ();
	}

	void Update()
	{
		
		if(fading)
		{
			if(right)
			{
				if (currentAttenuation < 0) {
					currentAttenuation += strenght;
					mixer.SetFloat ("MasterVolume", currentAttenuation);
				} 
				else 
				{
					fading = false;
					left = true;
					right = false;
				}
			}

			if(left)
			{
				if (currentAttenuation > maxAttenuation) {
					currentAttenuation -= strenght;
					mixer.SetFloat ("MasterVolume", currentAttenuation);
				} 
				else 
				{
					fading = false;
					right = true;
					left = false;
				}
			}
		}
	}

	void Fade()
	{
		fading = true;
	}

}
