﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {

	public Dialog dialog;

	// Use this for initialization
    void OnTriggerEnter()
    {
        if (dialog.isRunning())
            dialog.startDialog();
    }
}
