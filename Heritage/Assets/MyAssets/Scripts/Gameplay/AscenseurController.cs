﻿using UnityEngine;
using System.Collections;
using System;

public class AscenseurController : MonoBehaviour {

	public float force;
	public int min, max;
	public Transform topFloor;
	public float decalage;

	public Transform chariot;

	private bool isTopFloor;
	private bool isOnAsc;
	private bool isTrembling;

	private DateTime dateOld;

	private System.Random r;
	int getRandomNumber()
	{
		return r.Next(min, max);
	}

	// Use this for initialization
	void Start () {
		r = new System.Random ();

		dateOld = DateTime.Now;

		isTopFloor = false;
		isOnAsc = false;
		isTrembling = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			isOnAsc = true;
        }
	}

	void OnCollisionExit2D(Collision2D coll)
	{
        if (coll.gameObject.tag == "Player")
        {
            isOnAsc = false;
        }
    }

	void FixedUpdate () {


		if (isOnAsc && transform.position.y < topFloor.position.y) {		

            if (this.GetComponent<Rigidbody2D>().velocity.y == 0)
            {
                if (TestAction())
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, force);
                }
            } else
            {
                print("Debug : enter else");
                if (TestRightAction())
                {
                    print("Debug : enter if");
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    
                }
                isItShaking();
            }
            
            Shake();
		}
        else if (transform.position.y >= topFloor.position.y)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
	}

	void isItShaking(){		
		if (isTrembling) {
			print ("Debug : isItShaking enter ");
			chariot.position = new Vector3 (chariot.position.x - decalage, chariot.position.y, chariot.position.z);
		}
	}

	void Shake()
	{
		DateTime now = DateTime.Now;
		TimeSpan diff = now.Subtract (dateOld);

		int rng = getRandomNumber ();

		if (diff.TotalSeconds > 2)
			isTrembling = false;

		if (diff.TotalSeconds > rng) {
			isTrembling = true;
			EventHandler.TriggerEvent (EventHandler.GameEvent.MakeShake);
			dateOld = now;
		}




	}

	bool TestAction()
	{
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.JoystickButton4))
			return true;
		return false;
	}

	bool TestRightAction()
	{
		if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.JoystickButton5))
			return true;
		return false;
	}



}



