﻿using UnityEngine;
using System.Collections;

public class Lac : MonoBehaviour {

	private float initGravitScale;

	public Rigidbody2D rbPlayer;

	private Collider2D lacCollider;

	public float etatLacInit;
	public float etatLac;
	public float maxVelocity;

	public Renderer craquementSprite;

	// Disable gravity when the object is grounded


	void Start ()
	{
		etatLac = etatLacInit;
		lacCollider = GetComponent<Collider2D> ();

		if (craquementSprite)
		{
			Color col = craquementSprite.material.color;
			col.a = 0;
            craquementSprite.material.color = col;
		}
	}

	void FixedUpdate ()
	{
		if (!rbPlayer)
			return;

		bool isGrounded = rbPlayer.IsTouching(lacCollider);
		float playerVelocity = rbPlayer.velocity.magnitude;

		if (isGrounded && playerVelocity > maxVelocity)
		{

			etatLac -= Time.deltaTime;

			if (craquementSprite)
			{
				Color col = craquementSprite.material.color;
				col.a += (Time.deltaTime / etatLacInit);
                craquementSprite.material.color = col;
			}

			if (etatLac < 0)
			{
				// Lose : TODO
				EventHandler.TriggerEvent (EventHandler.GameEvent.OnPlayerFail);
				EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
				EventHandler.TriggerEvent (EventHandler.GameEvent.Fading);
				Debug.Log("Perdu");
				etatLac = 0;
			}
		}


	}
}
