﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Linq;
using Assets.MyAssets.Scripts;

public class PlayerControllerDialogs : PlayerController {

    public DoubleArray[] dialogColor;

	public float time;

	public Text dispText;
	public Image fadeBackText;

	private int indexDialog;
	private int indexLetter;
	private float timer;
	private bool textAnim;
	private bool inFading;

    private Color[] colors = { new Color(0.51f, 0.92f, 0.55f),
        new Color(0.93f, 0.36f, 0.08f),
        new Color(1f, 0.37f, 0.97f),
        new Color(0.64f, 0.22f, 0.18f)};

    private enum colorsAvailable
    {
        gen1Son = 0,
        gen1Dad = 1,
        gen2Son = 2,
        gen2Dad = 0,
        gen3Son = 3,
        gen3Dad = 2,
    }
	 

	void Start()
	{
		indexDialog = -1;
		indexLetter = 0;

		timer = 0;
		textAnim = false;
		inFading = true;

		Color c = fadeBackText.color;
		c.a = 0;
		fadeBackText.color = c;
	}

	IEnumerator FadeIn()
	{
		for (int i = 0; i < 10; i++)
		{
			Color c = fadeBackText.color;
			c.a += 0.1f;
			c.a = Mathf.Clamp (c.a, 0.0F, 1.0F);
			fadeBackText.color = c;
			yield return new WaitForSeconds (0.1f);
			Debug.Log ("Fade in");
		}
		inFading = false;
	}

	IEnumerator FadeOut()
	{
		for (int i = 0; i < 10; i++)
		{
			Color c = fadeBackText.color;
			c.a -= 0.1f;
			fadeBackText.color = c;
			yield return new WaitForSeconds (0.1f);
			Debug.Log ("Fade out");
		}
		inFading = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (textAnim)
		{




			if (indexDialog == dialogColor.Length)
				return;
			
			if (indexDialog == 0)
				StartCoroutine ("FadeIn");
			if (inFading)
				return;
			
			timer += Time.deltaTime;
			string targetText = dialogColor[indexDialog].array[0];

            //if (Input.GetButtonDown("Fire1"))
            //	dispText.text = targetText;

            if (timer > time)
            {
                char newLetter = targetText[indexLetter];
                var builder = new StringBuilder(dispText.text);
                builder.Append(newLetter);
                dispText.text = builder.ToString();
                dispText.color = colors[(int)System.Enum.Parse(typeof(colorsAvailable), dialogColor[indexDialog].array[1])];

				timer = 0;
				indexLetter++;
				if (indexLetter == targetText.Length)
				{
					textAnim = false;
					indexLetter = 0;
				}

			}
		}
		else if (Input.GetButton("Fire1"))
		{
			textAnim = true;
			indexDialog++;
			indexLetter = 0;
			dispText.text = "";
			if (indexDialog == dialogColor.Length)
			{
				StartCoroutine ("FadeOut");
				EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
			}
		}
	}
}
