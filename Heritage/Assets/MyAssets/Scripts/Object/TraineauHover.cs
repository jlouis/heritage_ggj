﻿using UnityEngine;
using System.Collections;

public class TraineauHover : MonoBehaviour {

	public float hoverStrenght = 5f;
	Vector3 startPos;

	void Start()
	{
		startPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		float theta = Time.timeSinceLevelLoad / 2;
		float distance = hoverStrenght * Mathf.Sin(theta);
		transform.position = new Vector3(transform.position.x, transform.position.y  * distance,transform.position.z);
	}
}
