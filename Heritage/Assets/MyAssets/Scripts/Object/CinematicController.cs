﻿using UnityEngine;
using System.Collections;

public class CinematicController : MonoBehaviour {

	public Rigidbody2D chariot;
	public GameObject scene;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay2D(Collider2D col)
	{
		print ("Debug : enter ontrigger");
		if (col.gameObject.tag == "Player" && Chariot.hasFail == true)
			animateSuicide ();
		else
			animateChariotThrow ();
	}

	void animateSuicide ()
	{

	}

	void animateChariotThrow ()
	{
		print ("Debug : enter throw");
		chariot.velocity = new Vector2 (3, chariot.velocity.y);
		print ("Chariot vitesse x : " + chariot.velocity.x);

	}
}
