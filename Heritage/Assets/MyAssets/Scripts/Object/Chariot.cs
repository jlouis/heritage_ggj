﻿using UnityEngine;
using System.Collections;

public class Chariot : MonoBehaviour {

	public static bool hasFail = false;

	// Use this for initialization
	void Start ()
	{
		EventHandler.AddEventListener (EventHandler.GameEvent.OnPlayerFail, Fail);
		EventHandler.AddEventListener (EventHandler.GameEvent.OnChangeScene, ChangeScene);
		EventHandler.AddEventListener (EventHandler.GameEvent.OnChangeIteration, ChangeIteration);
		EventHandler.AddEventListener (EventHandler.GameEvent.OnFadingInFinished, FadeFinished);
	}

	void FadeFinished()
	{
		if (!Chariot.hasFail)
			return;
		killChariot ();
	}

	void killChariot()
	{
		if (gameObject)
			Destroy (gameObject);
	}

	void ChangeScene ()
	{

	}

	void ChangeIteration ()
	{
		//Chariot.hasFail = false;
	}

	void Fail ()
	{
		if (Chariot.hasFail)
			return;

		Chariot.hasFail = true;
	}
}
