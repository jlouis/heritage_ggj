﻿using UnityEngine;
using System.Collections;

public class NoGravityGrounded : MonoBehaviour {

	private bool isGrounded;
	private float initGravitScale;

	private Rigidbody2D rb;

	// Disable gravity when the object is grounded


	void Start ()
	{
		isGrounded = false;
		rb = GetComponent<Rigidbody2D> ();
		initGravitScale = rb.gravityScale;
	}
	
	void FixedUpdate ()
	{

		if (isGrounded)
		{
			rb.gravityScale = 0;
		}
		else
			rb.gravityScale = initGravitScale;
	
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "level")
		{
			isGrounded = true;
		}
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "level")
		{
			isGrounded = false;
		}
	}
}
