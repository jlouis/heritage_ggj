﻿using UnityEngine;
using System.Collections;

public class SpriteTexLinearMovement : MonoBehaviour {

	public float speed = 1.0f;

	private Renderer render;

	void Start()
	{
		render = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float time = Time.timeSinceLevelLoad;
		render.sharedMaterial.mainTextureOffset = new Vector2 (time * speed, 0);
	}
}
