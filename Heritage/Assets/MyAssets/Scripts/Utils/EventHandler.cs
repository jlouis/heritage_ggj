﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void Callback();

public class EventHandler : Singleton<EventHandler>
{
    public enum GameEvent
    {
        Fading,
		OnFadingInFinished,
		OnFadingOutFinished,
		OnChangeScene,
		OnChangeIteration,
		OnPlayerFail,
		MakeShake
    }

    private static Dictionary<GameEvent, Delegate> gameEvents = new Dictionary<GameEvent, Delegate>();

    public static void AddEventListener(GameEvent key, Callback value)
    {
        if (!gameEvents.ContainsKey(key))
        {
            gameEvents.Add(key, null);
        }

        gameEvents[key] = (Callback)gameEvents[key] + value;
    }

    public static void RemoveEventListener(GameEvent key, Callback handler)
    {
        Delegate d;

        if (gameEvents.TryGetValue(key, out d))
        {
            gameEvents[key] = (Callback)gameEvents[key] - handler;
        }
    }

    public static bool TriggerEvent(GameEvent e)
    {
        Delegate d;

        if (gameEvents.TryGetValue(e, out d))
        {
            Callback callback = d as Callback;

            if (callback != null)
            {
                callback();

                return true;
            }
        }

        return false;
    }
}
