﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Dialog : MonoBehaviour {

	[System.Serializable]
	public class Word
	{
		public string msg;
		public float time;
	}

	IEnumerator FadeIn()
	{
		for (int i = 0; i < 10; i++)
		{
			Color c = backTxt.color;
			c.a += 0.1f;
			c.a = Mathf.Clamp (c.a, 0.0F, 1.0F);
			backTxt.color = c;
			yield return new WaitForSeconds (0.1f);
			Debug.Log ("Fade in");
		}
		inFading = false;
	}

	IEnumerator FadeOut()
	{
		for (int i = 0; i < 10; i++)
		{
			Color c = backTxt.color;
			c.a -= 0.1f;
			backTxt.color = c;
			yield return new WaitForSeconds (0.1f);
			Debug.Log ("Fade out");
		}
		inFading = false;
	}

	public Text txt;
	public Image backTxt;
	public Word[] dialog;

	private float timer;
	private int dialogIndex;
	private bool run;
	private bool inFading;

	// Use this for initialization
	void Start ()
	{
		run = false;
		backTxt.enabled = false;
		inFading = false;

		if (dialog.Length == 0)
			startDialog ();
	}

    public bool isRunning()
    {
        return run;
    }

	public void startDialog()
	{
		run = true;
		timer = 0;
		dialogIndex = 0;
		txt.text = dialog [0].msg;
		backTxt.enabled = true;
		inFading = true;
		StartCoroutine ("FadeIn");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (run && !inFading)
		{
			Word currentWord = dialog [dialogIndex];


			timer += Time.deltaTime;

			if (timer > currentWord.time)
			{
				timer = 0;
				dialogIndex++;
				if (dialogIndex == dialog.Length) {
					run = false;
					txt.text = "";
					//backTxt.enabled = false;
					StartCoroutine ("FadeOut");
				} else {
					txt.text = dialog [dialogIndex].msg;
				}
			}
		}
	}
}
