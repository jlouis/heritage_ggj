﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum FadingType {White, Black};

public class ScreenFader : MonoBehaviour
{
	enum FadingDir
	{
		In,
		Out
	}

	public FadingType fadingColor;
	public Image FadeBlackImg;
	public Image FadeWhiteImg;
	public float fadeSpeed = 1.5f;


	bool isFading = true;

	FadingDir direction = FadingDir.In;
	
	
	void Awake()
	{

	}

	void Start()
	{
		EventHandler.AddEventListener(EventHandler.GameEvent.Fading, LaunchFading);
		ChooseColor (fadingColor);
	}

	void Update()
	{
		if (isFading) 
		{
			switch(direction)
			{
				case FadingDir.In:
					FadingIn();
				break;

				case FadingDir.Out:
					FadingOut();
				break;
			}
		}
	}

	void LaunchFading()
	{
		isFading = true;
	}

	void FadingIn()
	{
		ChooseColor (MiniSceneHandler.curent.color);

		// Fade the texture to clear.
		FadeColorIn ();
		
		// If the texture is almost clear...
		if (FadeBlackImg.color.a <= 0.01f && FadeWhiteImg.color.a <= 0.01f)
		{
			// ... set the colour to clear and disable the RawImage.
			FadeBlackImg.gameObject.SetActive(false);
			FadeWhiteImg.gameObject.SetActive(false);
			isFading = false;
			// The scene is no longer starting.
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnFadingInFinished);
			direction = FadingDir.Out;
		}
	}
	
	
	public void FadingOut()
	{
		ChooseColor (MiniSceneHandler.curent.color);
		
		// Start fading towards black.
		FadeColorOut();
		
		// If the screen is almost black...
		if (FadeBlackImg.color.a >= 0.99f && FadeWhiteImg.color.a >= 0.99f) {

			//FadeBlackImg.gameObject.SetActive(false);
			//FadeWhiteImg.gameObject.SetActive(false);
			isFading = false;

			EventHandler.TriggerEvent (EventHandler.GameEvent.OnFadingOutFinished);
			direction = FadingDir.In;
		}

	}

	void ChooseColor(FadingType color)
	{
		switch (color) 
		{
			case FadingType.Black:
			FadeBlackImg.gameObject.SetActive(true);
			FadeWhiteImg.gameObject.SetActive(false);
				break;
				
			case FadingType.White:
			FadeWhiteImg.gameObject.SetActive(true);
			FadeBlackImg.gameObject.SetActive(false);
				break;
		}
	}

	void FadeColorIn()
	{
		FadeWhiteImg.color = Color.Lerp(FadeWhiteImg.color, new Vector4(FadeWhiteImg.color.r,
		                                                                FadeWhiteImg.color.g,
		                                                                FadeWhiteImg.color.b,
		                                                      0), 
		                           fadeSpeed * Time.deltaTime);

		FadeBlackImg.color = Color.Lerp(FadeBlackImg.color, new Vector4(FadeBlackImg.color.r,
		                                                                FadeBlackImg.color.g,
		                                                                FadeBlackImg.color.b,
		                                                                0), 
		                                fadeSpeed * Time.deltaTime);
	}

	void FadeColorOut()
	{
		FadeWhiteImg.color = Color.Lerp(FadeWhiteImg.color, new Vector4(FadeWhiteImg.color.r,
		                                                                FadeWhiteImg.color.g,
		                                                                FadeWhiteImg.color.b,
		                                                                1), 
		                                fadeSpeed * Time.deltaTime);
		
		FadeBlackImg.color = Color.Lerp(FadeBlackImg.color, new Vector4(FadeBlackImg.color.r,
		                                                                FadeBlackImg.color.g,
		                                                                FadeBlackImg.color.b,
		                                                                1), 
		                                fadeSpeed * Time.deltaTime);
	}
} 