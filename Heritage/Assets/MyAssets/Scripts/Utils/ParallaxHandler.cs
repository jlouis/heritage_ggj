﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallaxHandler : MonoBehaviour {
	private static ParallaxHandler _instance;
	public static ParallaxHandler Instance {
		get { return _instance; }
	}

	[System.Serializable]
	public class ParallaxPlaneInfos {
		public ParallaxPlane plane;
		public float speed;

		public ParallaxPlaneInfos(ParallaxPlane p, float s) {
			plane = p;
			speed = s;
		}
	}

	public Transform relativeTo;
	public bool ignoreY = true;
	public List<ParallaxPlaneInfos> planes;

	private Vector3 lastPosition;
	public Camera mainCamera;

	void Awake() {
		_instance = this;
	}

	void Start() {
		relativeTo = mainCamera.transform;
		lastPosition = relativeTo.position;
	}

	void LateUpdate () {
		Vector3 pos = relativeTo.position;

		if(lastPosition != pos) 
			UpdatePlanesPositions();
	}

	private void UpdatePlanesPositions() {
		Vector3 diff = relativeTo.position - lastPosition;

		if(ignoreY)
			diff.y = 0;

		foreach(ParallaxPlaneInfos planeInfo in planes) {
			planeInfo.plane.transform.Translate(diff * planeInfo.speed);
		}

		lastPosition = relativeTo.position;
	}
}
