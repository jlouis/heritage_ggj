﻿using UnityEngine;
using System.Collections;

public class MiniSceneEnd : MonoBehaviour {

	private bool isPlayer = false;
	private bool isTraineau = false;


	void Start()
	{
		isPlayer = false;
		isTraineau = false;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		
		if (col.tag == "Player") 
		{
			isPlayer = true;
			print ("Player");
		}
		if (col.tag == "traineau")
		{
			isTraineau = true;
			print ("Traineau");
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "Player") 
			
		{
			isPlayer = false;
			print ("Debug : Exit player");
		}
		if (col.tag == "traineau")
		{
			isTraineau = false;
			print ("Debug : Exit traineau");
		}
	}

	void FixedUpdate()
	{
		if (Chariot.hasFail && isPlayer)
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
		else if (isPlayer && isTraineau)
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
	}

}
