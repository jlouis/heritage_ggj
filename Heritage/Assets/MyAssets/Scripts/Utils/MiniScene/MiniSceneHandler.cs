﻿using UnityEngine;
using System.Collections.Generic;

public class MiniSceneHandler : Singleton<MiniSceneHandler> {

	public int nbMiniScenesByIteration;
	public List<MiniScene> miniScenes;
	int currentMiniScene = 0;
	int currentIteration = 0;
	bool changing = false;


	public static MiniScene curent {
		get;
		set;
	}

	// Use this for initialization
	void Start () 
	{
		EventHandler.AddEventListener(EventHandler.GameEvent.OnChangeScene, ChangeScene);
		EventHandler.AddEventListener(EventHandler.GameEvent.OnFadingOutFinished, FadeOutFinished);
		EventHandler.AddEventListener(EventHandler.GameEvent.OnFadingInFinished, FadeInFinished);

		foreach (MiniScene G in miniScenes) 
		{
			G.gameObject.SetActive(false);
		}

		miniScenes[0].gameObject.SetActive(true);
		curent = miniScenes [0];
	}

	void ChangeScene()
	{
		changing = true;
		EventHandler.TriggerEvent (EventHandler.GameEvent.Fading);
	}

	void UnloadMiniScenes()
	{
		curent.gameObject.SetActive(false);
	}

	void LoadNextMiniScene()
	{
		currentMiniScene++;
		curent = miniScenes [currentMiniScene];
		curent.gameObject.SetActive(true);
		EventHandler.TriggerEvent (EventHandler.GameEvent.Fading);

		if (currentMiniScene >= currentIteration * nbMiniScenesByIteration)
		{
			currentIteration++;
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeIteration);
		}
	}
	
	void FadeOutFinished()
	{
		if (changing) {
			UnloadMiniScenes ();
			LoadNextMiniScene ();

		}
		else
		{
			EventHandler.TriggerEvent (EventHandler.GameEvent.Fading);
		}
	}

	void FadeInFinished()
	{
		changing = false;
	}
}
