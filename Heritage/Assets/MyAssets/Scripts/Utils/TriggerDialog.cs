﻿using UnityEngine;
using System.Collections;

public class TriggerDialog : MonoBehaviour {

	public Dialog dialog;

	// Use this for initialization
    void OnTriggerEnter()
    {
        if (dialog.isRunning())
            dialog.startDialog();
    }
}
